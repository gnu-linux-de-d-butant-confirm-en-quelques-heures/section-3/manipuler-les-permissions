#Creation de l'arborescence
mkdir /tmp/exercices
mkdir /tmp/exercices/livres
mkdir /tmp/exercices/animaux
mkdir /tmp/exercices/GNU
mkdir /tmp/exercices/GNU/linux
mkdir /tmp/exercices/televisions
mkdir /tmp/exercices/ventilateurs
mkdir /tmp/exercices/cuisines
mkdir /tmp/exercices/cafes
mkdir /tmp/exercices/cafes/colombie
mkdir /tmp/exercices/cafes/perou
mkdir /tmp/exercices/appartements
mkdir /tmp/exercices/pays

#Création des fichiers
touch /tmp/exercices/televisions/samsung /tmp/exercices/televisions/led /tmp/exercices/televisions/altea
touch /tmp/exercices/cuisines/IKEA
useradd robert -p robert
chown robert /tmp/exercices/televisions/samsung
useradd pedro -p pedro
touch /tmp/exercices/cafes/colombie/robusta.cafe /tmp/exercices/cafes/colombie/arabica.cafe 
chown pedro /tmp/exercices/cafes/colombie/robusta.cafe 
touch /tmp/exercices/appartements/villes.list /tmp/exercices/appartements/departements.list

touch /tmp/exercices/livres/CKAD.book
touch /tmp/exercices/livres/LPIC.pdf
touch /tmp/exercices/livres/.cache.epub

touch /tmp/exercices/animaux/chat
touch /tmp/exercices/animaux/chien
touch /tmp/exercices/animaux/cheval
touch /tmp/exercices/animaux/singe
touch -a -m -t 201512180130.09 /tmp/exercices/animaux/singe #Modifie la date du fichier
touch -a -m -t 201512200130.09 /tmp/exercices/animaux/chien #Modifie la date du fichier

cp /etc/passwd /tmp/exercices/GNU/linux/ubuntu.file
sed -i 's/backups/backup/g' /tmp/exercices/GNU/linux/ubuntu.file
touch /tmp/exercices/GNU/linux/debian.file
touch /tmp/exercices/GNU/linux/centos.file

chmod 654 /tmp/exercices/GNU/linux/ubuntu.file
chmod 711 /tmp/exercices/GNU/linux/debian.file
chmod 700 /tmp/exercices/GNU/linux/centos.file
