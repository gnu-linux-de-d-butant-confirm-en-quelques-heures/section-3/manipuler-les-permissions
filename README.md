# GNU/Linux de débutant à confirmé en quelques heures

Bonjour et bienvenue dans ce scénario créé par **Jordan ASSOULINE** pour vous aider à maîtriser les commandes permettant de manipuler les permissions des fichiers et des répertoires sur Linux.

Ainsi nous verrons en détail la commande `chmod`.

<br/>

# Introduction

Tout d'abord, je vous invite à vérifier que Docker est bien installé sur votre machine.
Pour cela tapez la commande :
`sudo apt update && sudo apt install docker docker.io -y`

Vous pouvez aussi utiliser le docker playground si vous ne souhaitez pas installer ces éléments sur votre propre machine :
https://labs.play-with-docker.com/

Pour lancer l'environnement de l'exercice, vous allez exécuter la commande :

`docker run -it --rm --name man jassouline/chmod:latest bash`

Pour sortir de l'environnement, il suffit de taper la commande `exit` ou de taper sur la combinaison de touches : `CTRL + C`

<br/>

# Exercice 1

Pour vérifier les permissions associées à un fichier, on peut utiliser la commande `ls -l` afin de vérifier les permissions, dans l'ordre, de :
- l'utilisateur propriétaire (**u**)
- les utilisateurs du groupe (**g**)
- les autres utilisateurs (**o**)

<br />

*La première lettre correspond à `d` s'il s'agit d'un répertoire*

**Q1: Quelles sont les actions autorisées pour le propriétaire du fichier : /tmp/exercices/GNU/linux/ubuntu.file**
- [ ] Lecture
- [ ] Ecriture
- [ ] Exécution
- [ ] Aucun

<details><summary> Montrer la solution </summary>
<p>
ls -l /tmp/exercices/GNU/linux/ubuntu.file
</p>
<p>
On retrouve les permissions de lecture et d'écriture.
</p>
</details>

**Q2: Quelles sont les actions autorisées pour les autres utilisateurs du fichier : /tmp/exercices/GNU/linux/centos.file**
- [ ] Lecture
- [ ] Ecriture
- [ ] Exécution
- [ ] Aucun

<details><summary> Montrer la solution </summary>
<p>
ls -l /tmp/exercices/GNU/linux/centos.file
</p>
<p>
On ne retrouve aucune action autorisée pour les autres utilisateurs.
</p>
</details>

**Q3: Quelles sont les actions autorisées pour les utilisateurs du groupe "root" pour le fichier : /tmp/exercices/GNU/linux/debian.file**
- [ ] Lecture
- [ ] Ecriture
- [ ] Exécution
- [ ] Aucun

<details><summary> Montrer la solution </summary>
<p>
ls -l /tmp/exercices/GNU/linux/debian.file
</p>
<p>
On retrouve les permissions d'exécution.
</p>
</details>

<br/>

# Exercice 2

Dans ce deuxième exercice, nous allons utiliser la commande `chmod` pour modifier les permissions sur nos fichiers.

## Quelques informations avant de débuter

LPour vérifier les permissions associées à un fichier, on peut utiliser la commande `ls -l` afin de vérifier les permissions, dans l'ordre, de :
- l'utilisateur propriétaire (**u**)
- les utilisateurs du groupe (**g**)
- les autres utilisateurs (**o**)

*La première lettre correspond à `d` s'il s'agit d'un répertoire*

On effectue alors la commande : `chmod u+x nom_fichier` pour ajouter les droits d'exécution à l'utilisateur propriétaire du fichier.

On peut également retirer un droit existant, comme par exemple `chmod g-w nom_fichier` qui enlève les droits d'écriture pour les utilisateurs appartenant au groupe indiqué dans la commande `ls -l`.

## A vous de jouer !

### Modifier les permissions du fichier /tmp/exercices/GNU/linux/ubuntu.file

Modifier les permissions du fichier selon les paramètres suivants:
- Propriétaire: *lecture, écriture, exécution*
- Groupe root: *lecture, écriture*
- Autres utilisateurs: *lecture*

<details><summary> Montrer la solution </summary>
<p>
chmod 764 /tmp/exercices/GNU/linux/ubuntu.file
</p>
<p>
ls -l /tmp/exercices/GNU/linux/ubuntu.file
</p>
</details>

### Modifier les permissions du fichier /tmp/exercices/GNU/linux/centos.file

Modifier les permissions du fichier selon les paramètres suivants:
- Propriétaire: *lecture, écriture, exécution*
- Groupe root: *écriture*
- Autres utilisateurs: *lecture*
  
<details><summary> Montrer la solution </summary>
<p>
chmod 724 /tmp/exercices/GNU/linux/centos.file
</p>
<p>
ls -l /tmp/exercices/GNU/linux/centos.file
</p>
</details>

### Verification

1. Pour vérifier que vous avez correctement réussi cet exercice, tapez la commande suivante dans votre terminal :
`/tmp/change_verify.sh`

Si vous obtenez le message : `OK !` c'est que les actions ont été correctement effectuées.

<br/>

# Exercice 3

Dans ce dernier exercice, nous allons créer un script et le rendre exécutable sur le système.

## Création d'un script exécutable simple

Dans cet exercice, nous allons commencer par créer un script appelé **hello_world.sh**.

**Etape 1**: Créez un fichier vide appelé hello_world.sh

<details><summary> Montrer la solution </summary>
<p>
touch hello_world.sh
</p>
</details>

**Etape 2**: Editez le fichier **hello_world.sh** avec **nano** pour écrire à l'intérieur :
`echo 'Hello World'`

<details><summary> Montrer la solution </summary>
<p>
echo "echo 'Hello World'" > hello_world.sh
</p>
</details>

**Etape 3**: Essayez d'exécutez le script avec la commande `./hello_world.sh`

Malheureusement vous n'avez pas les droits d'exécution. Il est ainsi impossible d'exécuter ce script.
Nous allons donc ajouter ces droits.

**Etape 4**: Ajoutez les droits d'exécution pour le propriétaire du fichier, les membres du groupe, et les autres utilisateurs.

<details><summary> Montrer la solution </summary>
<p>
chmod a+x hello_world.sh
</p>
</details>

**Etape 5**: Essayez à nouveau d'exécuter le script.

<br/>

# Conclusion

Tapez la commande `exit` pour sortir du conteneur, ou bien la combinaison de touches `CTRL + c`

